/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCErrorResponse, SCRequests, SCResponses} from '@openstapps/core';

/**
 * A HTTP client that can send requests for the StApps API
 */
export interface HttpClientInterface {
  /**
   * Send request
   *
   * @param request Request to send
   */
  request<T extends SCResponses>(request: HttpClientRequest): Promise<HttpClientResponse<T>>;
}

/**
 * A map of headers
 */
export interface HttpClientHeaders {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: any;
}

/**
 * A HTTP client request
 */
export interface HttpClientRequest {
  /**
   * Body of the request
   *
   * A body to send with the request.
   */
  body?: SCRequests;

  /**
   * Headers of the request
   *
   * A key-value-map of headers to send with the request.
   */
  headers?: HttpClientHeaders;

  /**
   * Method of the request
   *
   * Should default to 'GET' if nothing is specified.
   */
  method?: 'GET' | 'POST' | 'PUT';

  /**
   * URL of the request
   *
   * The url to send the request to.
   */
  url: URL;
}

/**
 * A HTTP client response
 */
export interface HttpClientResponse<T extends SCResponses> {
  /**
   * Body of the response
   */
  body: T | SCErrorResponse;

  /**
   * Headers of the response
   *
   * A key-value-map of headers of the response
   */
  headers: HttpClientHeaders;

  /**
   * Status code of the response
   */
  statusCode: number;
}
