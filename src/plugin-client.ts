/*
 * Copyright (C) 2019-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCPluginRegisterRequest, SCPluginRegisterRoute} from '@openstapps/core';
import {ConnectorClient} from './connector-client';
import {Plugin} from './plugin';

/**
 * The PluginClient for registering and unregistering HTTP Plugins
 *
 * It contains a lot of the boilerplate for creating plugins, and thus simplifies the creation of such.
 */
export class PluginClient extends ConnectorClient {
  /**
   * Register a plugin in the backend
   *
   * **This method automatically calls [[Plugin.start]]**
   * You need to call this method before you can do anything with the plugin. If you want to register the plugin again,
   * you might first want to inform yourself how the backend behaves in such cases TODO: add docs for this
   *
   * @param plugin The instance of the plugin you want to register
   */
  async registerPlugin(plugin: Plugin) {
    const request: SCPluginRegisterRequest = {
      action: 'add',
      plugin: {
        address: plugin.fullUrl,
        name: plugin.name,
        requestSchema: plugin.requestSchema,
        responseSchema: plugin.responseSchema,
        route: plugin.route,
      },
    };
    await this.invokeRoute(new SCPluginRegisterRoute(), undefined, request);

    // start the plugin we just registered
    plugin.start();
  }

  /**
   * Unregister a plugin from the backend
   *
   * **This method automatically calls [[Plugin.stop]]**
   * If you want to unregister your plugin for some reason, you can do so by calling this method.
   * Use with caution.*
   *
   * @param plugin The instance of the plugin you want to register
   */
  async unregisterPlugin(plugin: Plugin) {
    const request: SCPluginRegisterRequest = {
      action: 'remove',
      route: plugin.route,
    };
    // stop the plugin we want to unregister
    plugin.stop();

    await this.invokeRoute(new SCPluginRegisterRoute(), undefined, request);
  }
}
