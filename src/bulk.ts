/*
 * Copyright (C) 2018-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {
  SCBulkAddResponse,
  SCBulkAddRoute,
  SCBulkDoneResponse,
  SCBulkDoneRoute,
  SCBulkResponse,
  SCThings,
  SCThingType,
} from '@openstapps/core';
import {Client} from './client';
import {BulkWithMultipleTypesError} from './errors';

/**
 * A bulk
 *
 * **!!! Bulk should only be instantiated by Client !!!**
 */
export class Bulk<T extends SCThings> {
  /**
   * Instance of multi search request route
   */
  private readonly bulkAddRoute = new SCBulkAddRoute();

  /**
   * Instance of multi search request route
   */
  private readonly bulkDoneRoute = new SCBulkDoneRoute();

  /**
   * **!!! Bulk should only be instantiated by Client !!!**
   *
   * @see Client.bulk
   */
  constructor(
    private readonly type: SCThingType,
    private readonly client: Client,
    private readonly bulkResponse: SCBulkResponse,
  ) {
    // noop
  }

  /**
   * Add a thing to the bulk
   *
   * @param thing Thing to add to the bulk
   */
  async add(thing: T): Promise<SCBulkAddResponse> {
    // check that thing has same type as bulk
    if (this.type !== thing.type) {
      throw new BulkWithMultipleTypesError(thing);
    }

    return this.client.invokeRoute<SCBulkAddResponse>(
      this.bulkAddRoute,
      {
        UID: encodeURIComponent(this.bulkResponse.uid),
      },
      thing,
    );
  }

  /**
   * Declare this bulk transfer as done
   *
   * This will activate the index in the backend and possibly delete old data. There are many potential processing steps
   * required in the backend so it might take a few seconds before the callback is called.
   */
  async done(): Promise<SCBulkDoneResponse> {
    return this.client.invokeRoute<SCBulkDoneResponse>(
      this.bulkDoneRoute,
      {
        UID: this.bulkResponse.uid,
      },
      {},
    );
  }
}
