/*
 * Copyright (C) 2018-2022 Open StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCErrorResponse, SCLicensePlate, SCSearchRequest, SCThings} from '@openstapps/core';

/**
 * An error that can occur in the StApps API
 */
export class ApiError extends Error {
  /**
   * Instantiate a new error
   *
   * @param data Representation of an error that happened in the backend
   */
  constructor(protected data: Partial<SCErrorResponse>) {
    super(data.message);

    if (typeof data.name === 'string') {
      this.name = data.name;
    }
  }

  /**
   * Add additional data to the output of the error
   */
  toString(): string {
    let string_ = super.toString();

    // add additional data
    if (typeof this.data.additionalData !== 'undefined') {
      string_ += `\n\n${JSON.stringify(this.data.additionalData)}`;
    }

    // add "remote" stack trace
    if (typeof this.data.stack !== 'undefined') {
      string_ += `\n\n${this.data.stack}`;
    }

    return string_;
  }
}

/**
 * Error that is thrown if the next window is beyond the total number of results
 */
export class OutOfRangeError extends ApiError {
  /**
   * Instantiate a new error
   *
   * @param searchRequest Search request where window is out of range
   */
  constructor(searchRequest: SCSearchRequest) {
    super({
      additionalData: searchRequest,
      message: 'The next window is beyond the total number of results.',
      name: 'OutOfRangeError',
    });
  }
}

/**
 * Error that is thrown when a bulk is filled with things of multiple types
 */
export class BulkWithMultipleTypesError extends ApiError {
  /**
   * Instantiate a new error
   *
   * @param offendingThing Thing that has a different type than the previous things
   */
  constructor(offendingThing: SCThings) {
    super({
      additionalData: offendingThing,
      message: 'A bulk can only contain one type of things!',
      name: 'BulkWithMultipleTypesError',
    });
  }
}

/**
 * Error that is thrown when a bulk is empty
 */
export class EmptyBulkError extends ApiError {
  /**
   * Instantiate a new error
   */
  constructor() {
    super({
      message: 'You can not fill a bulk with zero things.',
      name: 'EmptyBulkError',
    });
  }
}

/**
 * Error that is thrown when there is no namespace for a license plate
 */
export class NamespaceNotDefinedError extends ApiError {
  /**
   * Instantiate a new error
   */
  constructor(namespaceId: SCLicensePlate) {
    super({
      message: `'${namespaceId}' has no namespace defined`,
      name: 'NamespaceNotDefinedError',
    });
  }
}

/**
 * Error that is thrown when API and backend StAppsCore versions are incompatible
 */
export class CoreVersionIncompatibleError extends ApiError {
  /**
   * Instantiate a new error
   */
  constructor(localVersion: string, remoteVersion: string) {
    super({
      message: `Local StAppsCore version ${localVersion} is incompatible to remote version ${remoteVersion}.`,
      name: 'CoreVersionIncompatibleError',
    });
  }
}

/**
 * Error that is thrown when API and backend StAppsCore versions are incompatible
 */
export class PluginNotAvailableError extends ApiError {
  /**
   * Instantiate a new error
   */
  constructor(requestedPluginWithName: string) {
    super({
      message: `A plugin named ${requestedPluginWithName} is not available.`,
      name: 'PluginNotAvailable',
    });
  }
}
