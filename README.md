# @openstapps/api

Node.js library to interact with the StApps backend service

## Use this as a standalone program

To get some data into a local `backend-node`-instance, you can run this
as a standalone program to copy data of a remote `backend-node`-instance
into your local one.

Example to copy all Events of the b-tu instance:

```shell
npm install
npm run build
node ./lib/cli.js copy Event https://stappsbe01.innocampus.tu-berlin.de http://localhost:3000 100
```

Example to index all items from @openstapps/core test files to a backend:

```shell
npm install
npm run build
node ./lib/cli.js e2e http://localhost:3000
```

### Program arguments

```shell
node ./lib/cli.js copy <type> <from> <to> <batchSize>

node ./lib/cli.js e2e <to>
```

#### Options

The source identifier for the bulk to use with the target instance (default is 'copy')

```shell
-s, --bulkSource <bulkScource>
```

The App version to use (unset by default)

```shell
-a, --appVersion <version>
```

The only available option for `e2e` command. File path to json test files each containing a SCThing.

```shell
-s, --samples <path>
```

### Example execution

with docker when backend is running on `localhost:3000`:

```shell
docker run --net=host registry.gitlab.com/openstapps/api/cli copy Place https://stappsbe01.innocampus.tu-berlin.de http://localhost:3000 100
```

Or using `e2e` command:

```shell
docker run --net=host registry.gitlab.com/openstapps/api/cli e2e http://localhost:3000
```
