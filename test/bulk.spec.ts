/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCBulkAddRoute, SCBulkDoneRoute, SCDish, SCMessage, SCThingOriginType, SCThingType} from '@openstapps/core';
import {expect} from 'chai';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import chaiSpies from 'chai-spies';
import {suite, test} from '@testdeck/mocha';
import moment from 'moment';
import {Bulk} from '../src/bulk';
import {Client} from '../src/client';
import {BulkWithMultipleTypesError} from '../src/errors';
import {HttpClient} from '../src/http-client';

chai.should();
chai.use(chaiSpies);
chai.use(chaiAsPromised);

const sandbox = chai.spy.sandbox();

const bulkAddRoute = new SCBulkAddRoute();
const bulkDoneRoute = new SCBulkDoneRoute();

const httpClient = new HttpClient();
const client = new Client(httpClient, 'http://localhost');

@suite()
export class BulkSpec {
  @test
  async add() {
    sandbox.on(client, 'invokeRoute', () => {
      return {};
    });

    expect(client.invokeRoute).not.to.have.been.called();

    const bulk = new Bulk(SCThingType.Dish, client, {
      expiration: moment().add(3600, 'seconds').format(),
      source: 'foo',
      state: 'in progress',
      type: SCThingType.Dish,
      uid: 'bar',
    });

    const dish: SCDish = {
      categories: [
        'main dish',
      ],
      name: 'foobar',
      origin: {
        indexed: moment().format(),
        name: 'bar',
        type: SCThingOriginType.Remote,
      },
      type: SCThingType.Dish,
      uid: 'foo',
    };

    await bulk.add(dish);

    expect(client.invokeRoute).to.have.been.first.called.with(bulkAddRoute, {
      UID: 'bar',
    }, dish);
  }

  @test
  async addFails() {
    const bulk = new Bulk(SCThingType.Dish, client, {
      expiration: moment().add(3600, 'seconds').format(),
      source: 'foo',
      state: 'in progress',
      type: SCThingType.Dish,
      uid: 'bar',
    });

    const message: SCMessage = {
      audiences: [
        'students',
      ],
      categories: [
        'news'
      ],
      messageBody: 'Lorem ipsum.',
      name: 'foobar',
      origin: {
        indexed: moment().format(),
        name: 'bar',
        type: SCThingOriginType.Remote,
      },
      type: SCThingType.Message,
      uid: 'foo',
    };

    return bulk.add(message).should.be.rejectedWith(BulkWithMultipleTypesError);
  }

  async after() {
    sandbox.restore();
  }

  @test
  async construct() {
    expect(() => {
      return new Bulk(SCThingType.Dish, client, {
        expiration: moment().add(3600, 'seconds').format(),
        source: 'foo',
        state: 'in progress',
        type: SCThingType.Dish,
        uid: 'bar',
      });
    }).not.to.throw();
  }

  @test
  async done() {
    sandbox.on(client, 'invokeRoute', () => {
      return {};
    });

    expect(client.invokeRoute).not.to.have.been.called();

    const bulk = new Bulk(SCThingType.Dish, client, {
      expiration: moment().add(3600, 'seconds').format(),
      source: 'foo',
      state: 'in progress',
      type: SCThingType.Dish,
      uid: 'bar',
    });

    await bulk.done();

    expect(client.invokeRoute).to.have.been.first.called.with(bulkDoneRoute, {
      UID: 'bar',
    });
  }
}
