/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCPluginRegisterRequest, SCPluginRegisterResponse, SCPluginRegisterRoute} from '@openstapps/core';
import chai from 'chai';
import {expect} from 'chai';
import chaiSpies from 'chai-spies';
import {suite, test, timeout} from '@testdeck/mocha';
import {HttpClient} from '../src/http-client';
import {HttpClientResponse} from '../src/http-client-interface';
import {PluginClient} from '../src/plugin-client';
import {TestPlugin} from './plugin-resources/test-plugin';

chai.use(chaiSpies);

const sandbox = chai.spy.sandbox();

const httpClient = new HttpClient();

const pluginRegisterRoute = new SCPluginRegisterRoute();

const pluginClient = new PluginClient(httpClient, 'http://localhost');

@suite(timeout(10000))
export class PluginClientSpec {
  static plugin: TestPlugin;

  static async after() {
    await this.plugin.close();
  }

  static async before() {
    this.plugin = new TestPlugin(4000, '', '', '', '', {getSchema: () => {/***/}} as any, '', '', '');
  }

  async after() {
    sandbox.restore();
  }

  @test
  async registerPlugin() {
    sandbox.on(httpClient, 'request', async (): Promise<HttpClientResponse<SCPluginRegisterResponse>> => {
      return {
        body: {
          success: true,
        },
        headers: {},
        statusCode: pluginRegisterRoute.statusCodeSuccess,
      };
    });

    expect(httpClient.request).not.to.have.been.called();

    await pluginClient.registerPlugin(PluginClientSpec.plugin);

    const request: SCPluginRegisterRequest = {
      action: 'add',
      plugin: {
        address: PluginClientSpec.plugin.fullUrl,
        name: PluginClientSpec.plugin.name,
        requestSchema: PluginClientSpec.plugin.requestSchema,
        responseSchema: PluginClientSpec.plugin.responseSchema,
        route: PluginClientSpec.plugin.route,
      },
    };

    expect(httpClient.request).to.have.been.first.called.with({
      body: request,
      headers: {
        "Content-Type": "application/json",
      },
      method: pluginRegisterRoute.method,
      url: new URL(`http://localhost${pluginRegisterRoute.getUrlPath()}`),
    });
  }

  @test
  async unregisterPlugin() {
    sandbox.on(httpClient, 'request', async (): Promise<HttpClientResponse<SCPluginRegisterResponse>> => {
      return {
        body: {
          success: true,
        },
        headers: {},
        statusCode: pluginRegisterRoute.statusCodeSuccess,
      };
    });

    expect(httpClient.request).not.to.have.been.called();

    await pluginClient.unregisterPlugin(PluginClientSpec.plugin);

    const request: SCPluginRegisterRequest = {
        action: 'remove',
        route: PluginClientSpec.plugin.route,
    };

    expect(httpClient.request).to.have.been.first.called.with({
      body: request,
      headers: {
        "Content-Type": "application/json",
      },
      method: pluginRegisterRoute.method,
      url: new URL(`http://localhost${pluginRegisterRoute.getUrlPath()}`),
    });
  }
}
