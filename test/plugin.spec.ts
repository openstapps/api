/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import {Converter} from '@openstapps/core-tools/lib/schema';
import chai from 'chai';
import {expect} from 'chai';
import chaiSpies from 'chai-spies';
import {readFileSync} from 'fs';
import {suite, test, timeout} from '@testdeck/mocha';
import {resolve} from 'path';
import {HttpClient} from '../src/http-client';
import {TestPlugin} from './plugin-resources/test-plugin';

chai.use(chaiSpies);

process.on('unhandledRejection', (err) => {
  throw err;
});

const sandbox = chai.spy.sandbox();

const httpClient = new HttpClient();

@suite(timeout(20000))
export class PluginSpec {
  static testPlugin: TestPlugin;

  static async after() {
    PluginSpec.testPlugin.close();
  }

  static async before() {
    PluginSpec.testPlugin = new TestPlugin(4000, '', '', '', '', {
      getSchema: () => {/***/
      },
    } as any, '', '', '');
  }

  async after() {
    sandbox.restore();
  }

  @test
  async construct() {
    const converter = new Converter(__dirname, resolve(__dirname,'plugin-resources','test-plugin-response.ts'));

    sandbox.on(converter, 'getSchema', (schemaName) => {
      return {$id: schemaName};
    });

    const constructTestPlugin = new TestPlugin(
      4001,
      'A',
      'http://B',
      '/C', // this doesn't matter for our tests, it's only something that affects the backend
      'http://D',
      // @ts-ignore fake converter is not a converter
      converter,
      'PluginTestRequest',
      'PluginTestResponse',
      JSON.parse(readFileSync(resolve(__dirname, '..', 'package.json')).toString()).version,
    );
    expect(constructTestPlugin.port).to.be.equal(4001);
    expect(constructTestPlugin.name).to.be.equal('A');
    expect(constructTestPlugin.url).to.be.equal('http://B');
    expect(constructTestPlugin.route).to.be.equal('/C');
    // @ts-ignore backendUrl is protected
    expect(constructTestPlugin.backendUrl).to.be.equal('http://D');
    // schemas are already covered, together with the directory and version
    // @ts-ignore active is private
    expect(constructTestPlugin.active).to.be.equal(false);
    expect(constructTestPlugin.requestSchema.$id).to.be.equal('PluginTestRequest');
    expect(constructTestPlugin.responseSchema.$id).to.be.equal('PluginTestResponse');

    sandbox.on(constructTestPlugin, 'onRouteInvoke');
    await httpClient.request({
      url: new URL('http://localhost:4001'),
    });
    // onRouteInvoke is a protected method, but we need to access it from the outside to test it
    // @ts-ignore
    expect(constructTestPlugin.onRouteInvoke).not.to.have.been.called();

    await constructTestPlugin.close();
    sandbox.restore(constructTestPlugin, 'onRouteInvoke');
  }

  @test
  async fullUrl() {
    const constructTestPlugin = new TestPlugin(4001, '', 'http://B', '', '', {
      getSchema: () => {/***/
      },
    } as any, '', '', '');
    expect(constructTestPlugin.fullUrl).to.be.equal('http://B:4001');
    await constructTestPlugin.close();
  }

  @test
  async start() {
    PluginSpec.testPlugin.start();

    sandbox.on(PluginSpec.testPlugin, 'onRouteInvoke');

    await httpClient.request({
      url: new URL('http://localhost:4000'),
    });

    // onRouteInvoke is a protected method, but we need to access it from the outside to test it
    // @ts-ignore
    expect(PluginSpec.testPlugin.onRouteInvoke).to.have.been.called();
  }

  @test
  async stop() {
    // simulate a normal use case by first starting the plugin and then stopping it
    PluginSpec.testPlugin.start();
    PluginSpec.testPlugin.stop();

    sandbox.on(PluginSpec.testPlugin, 'onRouteInvoke');

    const response = await httpClient.request({
      url: new URL('http://localhost:4000'),
    });

    await expect(response.statusCode).to.be.equal(404);
    // onRouteInvoke is a protected method, but we need to access it from the outside to test it
    // @ts-ignore
    expect(PluginSpec.testPlugin.onRouteInvoke).not.to.have.been.called();
  }
}
