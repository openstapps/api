/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

// tslint:disable-next-line: max-line-length
// tslint:disable: completed-docs no-implicit-dependencies prefer-function-over-method newline-per-chained-call member-ordering
import {
  SCBulkAddResponse,
  SCBulkAddRoute,
  SCBulkDoneResponse,
  SCBulkDoneRoute,
  SCBulkResponse,
  SCBulkRoute,
  SCSearchResponse,
  SCSearchRoute,
  SCThings,
} from '@openstapps/core';
import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import chaiSpies from 'chai-spies';
import clone = require('rfdc');
import {existsSync, mkdirSync, rmdirSync, unlinkSync} from 'fs';
import {createFileSync} from 'fs-extra';
import {suite, test} from '@testdeck/mocha';
import {join} from 'path';
import {e2eRun, getItemsFromSamples} from '../src/e2e';
import {ApiError} from '../src/errors';
import {HttpClient, RequestOptions, Response} from '../src/http-client';
import {RecursivePartial} from './client.spec';

chai.should();
chai.use(chaiSpies);
chai.use(chaiAsPromised);

const sandbox = chai.spy.sandbox();

const bulkRoute = new SCBulkRoute();
const bulkAddRoute = new SCBulkAddRoute();
const bulkDoneRoute = new SCBulkDoneRoute();

const searchRoute = new SCSearchRoute();

const httpClient = new HttpClient();

const storedThings: Map<string, SCThings> = new Map();

@suite
export class E2EConnectorSpec {
  async after() {
    sandbox.restore();
  }

  @test
  async getCoreTestSamples() {
    const items = await getItemsFromSamples('./node_modules/@openstapps/core/test/resources');
    // tslint:disable-next-line: no-unused-expression
    chai.expect(items).to.not.be.empty;
  }

  @test
  async getCoreTestSamplesShouldFail() {
    await chai.expect(getItemsFromSamples('./nonexistantdirectory')).to.be.rejectedWith(Error);
  }

  @test
  async e2eRunSimulation() {
    type responses = Response<SCBulkAddResponse | SCBulkDoneResponse | SCBulkResponse | SCSearchResponse>;

    let failOnCompare = false;
    let failOnLookup = false;

    sandbox.on(httpClient, 'request', async (request: RequestOptions): Promise<RecursivePartial<responses>> => {
      if (request.url.toString() === `http://localhost${bulkRoute.getUrlPath().toString()}`) {

        return {
          body: {
            state: 'in progress',
            uid: 'foo',
          },
          statusCode: bulkRoute.statusCodeSuccess,
        };
      }

      if (request.url.toString() === `http://localhost${bulkAddRoute.getUrlPath({UID: 'foo'}).toString()}`) {
        storedThings.set(request.body.uid, clone()(request.body));

        return {
          body: {},
          statusCode: bulkAddRoute.statusCodeSuccess,
        };
      }

      if (request.url.toString() === `http://localhost${bulkDoneRoute.getUrlPath({UID: 'foo'}).toString()}`) {
        return {
          body: {},
          statusCode: bulkDoneRoute.statusCodeSuccess,
        };
      }

      if (request.url.toString() === `http://localhost${searchRoute.getUrlPath().toString()}`) {
        const thing = storedThings.get(request.body.filter.arguments.value);
        if (failOnCompare) {
          thing!.origin!.modified = 'altered';
        }
        const returnThing = failOnLookup ? [] : [thing];
        const returnBody = {
          data: returnThing,
          facets: [],
          pagination: {
            count: returnThing.length,
            offset: 0,
            total: returnThing.length,
          },
          stats: {
            time: 42,
          },
        };

        return {
          body: returnBody,
          statusCode: searchRoute.statusCodeSuccess,
        };
      }

      return {
        body: {},
        statusCode: searchRoute.statusCodeSuccess,
      };
    });

    // tslint:disable-next-line: max-line-length
    await e2eRun(httpClient, {to: 'http://localhost', samplesLocation: './node_modules/@openstapps/core/test/resources'});

    failOnLookup = true;
    failOnCompare = false;
    // tslint:disable-next-line: max-line-length
    await e2eRun(httpClient, {to: 'http://localhost', samplesLocation: './node_modules/@openstapps/core/test/resources'})
    .should.be.rejectedWith('Search for single SCThing with uid');

    failOnLookup = false;
    failOnCompare = true;
    // tslint:disable-next-line: max-line-length
    await e2eRun(httpClient, {to: 'http://localhost', samplesLocation: './node_modules/@openstapps/core/test/resources'})
    .should.be.rejectedWith('Unexpected difference');

  }

  @test
  async indexShouldFail() {
    type responses = Response<SCBulkAddResponse | SCBulkDoneResponse | SCBulkResponse>;

    sandbox.on(httpClient, 'request', async (): Promise<RecursivePartial<responses>> => {

      return {
        body: {},
        statusCode: Number.MAX_SAFE_INTEGER,
      };
    });

    // tslint:disable-next-line: max-line-length
    return e2eRun(httpClient, {to: 'http://localhost', samplesLocation: './node_modules/@openstapps/core/test/resources'})
    .should.be.rejectedWith(ApiError);
  }

  @test
  async indexShouldFailDirectoryWithoutData() {
    const emptyDirPath = join(__dirname, 'emptyDir');
    if (!existsSync(emptyDirPath)) {
      mkdirSync(emptyDirPath);
    }
    await e2eRun(httpClient, {to: 'http://localhost', samplesLocation: emptyDirPath})
    .should.be.rejectedWith('Could not index samples. None were retrieved from the file system.');
    rmdirSync(emptyDirPath);
  }

  @test
  async indexShouldFailDirectoryWithoutJsonData() {
    const somewhatFilledDirPath = join(__dirname, 'somewhatFilledDir');
    if (!existsSync(somewhatFilledDirPath)) {
      mkdirSync(somewhatFilledDirPath);
    }
    const nonJsonFile = join (somewhatFilledDirPath, 'nonjson.txt');
    createFileSync(nonJsonFile);
    await e2eRun(httpClient, {to: 'http://localhost', samplesLocation: somewhatFilledDirPath})
    .should.be.rejectedWith('Could not index samples. None were retrieved from the file system.');
    unlinkSync(nonJsonFile);
    rmdirSync(somewhatFilledDirPath);
  }
}
