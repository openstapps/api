/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import chai from 'chai';
import {expect} from 'chai';
import chaiAsPromised from 'chai-as-promised';
import chaiSpies from 'chai-spies';
import {suite, test} from '@testdeck/mocha';
import {ApiError} from '../src/errors';

chai.should();
chai.use(chaiSpies);
chai.use(chaiAsPromised);

const sandbox = chai.spy.sandbox();

@suite()
export class ErrorsSpec {
  async after() {
    sandbox.restore();
  }

  @test
  async shouldAddAdditionalData() {
    const error = new ApiError({
      additionalData: 'Lorem ipsum',
    });

    expect(error.toString()).to.contain('Lorem ipsum');
  }

  @test
  async shouldAddRemoteStackTrace() {
    const error = new ApiError({
      stack: 'Lorem ipsum',
    });

    expect(error.toString()).to.contain('Lorem ipsum');
  }

  @test
  async shouldSetName() {
    const error = new ApiError({
      name: 'Foo',
    });

    expect(error.name).to.be.equal('Foo');
  }
}
