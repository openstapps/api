/*
 * Copyright (C) 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */

import * as express from 'express';
import {Plugin} from '../../src/plugin';

/**
 * A test plugin we use for all the tests
 *
 * It can be constructed without any parameter at all, or with all parameters if we want to test it
 * It also serves as kind of a minimal plugin
 */
export class TestPlugin extends Plugin {
  // tslint:disable-next-line: completed-docs prefer-function-over-method
  protected async onRouteInvoke(_req: express.Request, res: express.Response): Promise<void> {
    res.json({});

    return undefined;
  }
}
