FROM registry.gitlab.com/openstapps/projectmanagement/node

ADD . /app
WORKDIR /app

ENTRYPOINT ["node", "lib/cli.js"]

CMD ["--help"]
